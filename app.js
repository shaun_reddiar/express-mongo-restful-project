/**
 * This is the express application that will spin up with our server which will make
 * handling requests a bit easier for us.
 */
const express = require('express');

/**
 * This const executes express and allows us to access all of its functions available.
 */
const app = express();

// Import our routes
const productRoutes = require('./api/routes/products');
const orderRoutes = require('./api/routes/orders');
const userRoutes = require('./api/routes/user');

// Import morgan package to log activity
const morgan = require('morgan');

// We use body-parser package to extract the body from our requests as they are not easy to read with out it
const bodyParser = require('body-parser');

// Add mongoose for database related methods and tasks:
const mongoose = require('mongoose');



// Connect to our database in MongoDB - copy path copied in MongoDB
// For production you should probably set up the passowrd as an environment variable and enter like this:
//  process.env.MONGO_ATLAS_PW - thsi would allow us to not hardcode this value in our JS code. For now it has been added in our nodemon.json file
mongoose.connect('mongodb+srv://shaun_reddiar:' + process.env.MONGO_ATLAS_PW + '@restful-api-test-oz3vo.mongodb.net/<dbname>?retryWrites=true&w=majority',
    {
        useUnifiedTopology: true,
        useNewUrlParser: true
    }
);

// Removes deprecation warning
mongoose.Promise = global.Promise;


// Use Morgan package to log all requests
app.use(morgan('dev'));
// Make files in uploads folder publically accessible
app.use('/uploads', express.static('uploads'));
// Use Body parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Funnel all requests to add header info to prevent CORS errors
app.use((req, res, next) => {
    // What routes to accepts - Everything in most cases
    res.header('Access-Control-Allow-Origin', '*');
    // What headers to append to an incoming request
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization' );

    /**
     * We check if the incoming request method(The property which gives us access to the http method we use)
     * is equal to OPTIONS - A browser will always send an OPTIONS request first before any request, This
     * will always be the case, a browsers needs to check if it is allowed to make the request you are sending
     */

     if (req.method === 'OPTIONS') {
         res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
         return res.status(200).json({});
     }

     // Allow other middleware to carry on from here
     next()
})

/**
 * We first execute expresses use() method which is middleware.
 * An incoming request to the server has to go through app.use() and to
 * whatever we pass to it.
 * 
 * We pass an arrow function with 3 arguments:
 * req:     the request made
 * res:     the response from the server that needs to be handled
 * next:    A function that you use to move the request to the next
 *          middleware in line, if next is not added, no further middleware
 *          methods will be added to the chain
 * 
 * In the below code you will see that we use this to handle our routes as well
 */
// app.use((req, res, next) => {
//     // We set the status code and a json method which will send us a JSON object
//     // We pass an object into the json method with whatever message we want to pass
//     res.status(200).json({
//         message: 'Its works'
//     });
// });

// Adding routes for requests
/**
 * We use the use() middleware in express to catch all incoming requests
 * anything starting with /products will be forwarded to our productRoutes to handle
 */
app.use('/products', productRoutes);

/**
 * import orders resource
 */
app.use('/orders', orderRoutes);

/**
 * Import users resource
 */
app.use('/user', userRoutes)

// Error handle all requests not captured
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    // Send error to next middleware
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        message: error.message
    })
});



// Allow the app application to be imported and accessed via require() in the server.js (Our server)
module.exports = app;