const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const User = require('../models/user');
const jwt = require('jsonwebtoken');

router.post('/signup', (req, res, next) => {
    // Create a new user
    /**
     * We use bcrypt to has our password,
     * 1st arg is the string to hash
     * 2nd has is the Salt rounds to apply - THis would prevent our hashed string from being discovered on Google via a dictionary table
     * Callback where we get either an error or the hashed password
     */

     // First check if user exists

     User.find({email: req.body.email})
     .exec()
     .then(user => {
         if (user.length >= 1) {
             return res.status(422).json({
                 message: 'A user with that email already exists!'
             })
         } else {
             bcrypt.hash(req.body.password, 10, (err, hash) => {
                 if (err) {
                     return res.status(500).json({
                         error: err
                     });
                 } else {
                     const user = new User({
                         _id: new mongoose.Types.ObjectId(),
                         email: req.body.email,
                         password: hash
                     });
                     user.save()
                         .then(result => {
                             console.log(result);
                             res.status(201).json({
                                 message: 'User Created',
                             })
                         })
                         .catch(err => {
                             res.status(500).json({
                                 message: 'Error creating user',
                                 error: err
                             })
                         });
                 }
             });
         }
     });

});

/**
 * Log in a user
 */
router.post('/login', (req, res, next) => {
    // Use mongoose to find any user with the email address passed in the request
    User.find({email: req.body.email})
    .exec()
    .then(user => {
        // If nothing is returned, the user does not exist
        if (user.length < 1) {
            // Don't add specific information about email address or passowrds being wrong
            // as this would be a security issue to hackers looking for valid emails
            return res.status(401).json({
                message: 'Auth failed'
            })
        }

        // Make sure that the password sent by the user matches the hashed password stored on the database
        // We cant reverse a hashed password, so we will hash the request password with teh same algorithm
        // and see if the hashed passwords match.
        /**
         * bcrypt.compare(request password, stored user password, callback response)
         */
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if(err) {
                // And err does not happen if it does not find a macth but rather if the compare process fails
                return res.status(401).json({
                    message: 'Auth failed'
                })
            }

            if (result) {
                // Return JWT token here
                // We pass the details we want to include in the first arg object,
                // then we include our private - which we store in our env variable (nodemon.json)
                // Then we add our options
                const token = jwt.sign({
                    email: user[0].email,
                    userId: user[0]._id
                }, process.env.JWT_KEY, {
                    expiresIn: "1h"
                });

                return res.status(200).json({
                    message: 'Auth successful',
                    token: token
                });
            }

            return res.status(401).json({
                message: 'Auth failed'
            })
        })
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        })
    })

});

/**
 * Delete a user
 */
router.delete('/:userId', (req, res, next) => {
    // Use req.params.userId to get id encoded in our URL

    User.remove({_id: req.params.userId})
    .exec()
    .then(result => {
        res.status(200).json({
            message: 'User deleted'
        });
    })
    .catch(err => {
        res.status(500).json({
            error: err
        })
    })
});


module.exports = router;