/**
 * This file will handle our Product related routes.
 */

const express = require('express');

/**
 * Include Multer package to be able to pass form data
 */
const multer = require('multer');

// Set filter to only accept certain mimeTypes for file uploads
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        // Accept file
        cb(null, true);
    } else {
        // Reject file
        cb(null, false);
    }
};

// Adjust how files get stored by Multer
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + '-' + file.originalname);
    }
});

/**
 * Specify a folder where multer will try to store all incoming files
 * 
 * There will be a permission issue with this folder. To solve this go to app.js file
 * and we set this folder up to be statically accessible
 */
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5 // Only accept files sizes that are less than 5mb
    },
    fileFilter: fileFilter
});

/**
 * Import our Product schema
 */
const Product = require('../models/product');
const mongoose = require('mongoose');

/**
* A package Express ships with that allows us to conveniently handle routes
*/
const router = express.Router();

/**
 * Check auth on all requests
 */
const checkAuth = require('../middleware/check-auth'); 

/**
* We can now use Router to register different routes or resources.
* 
* /products
* GET - Get all products
* POST - add a new product
* 
* /products/{id}
* GET - fetch a single product resource
* PATCH - update or edit and existing product resource
* DELETE - delete a product resource
*/

/**
* router.get() is a method that will handle GET requests.
* 
* Just '/' is used, NOT '/products'. We don't need to include /products here  as this is already handled in our app.js file
* essentially this is at the point when the app will forward the request here BECAUSE a /products
* is included in the request. ie. Added /products here will result in localhost:3000/products/products being required
* to execute this code.
*/
router.get('/', (req, res, next) => {
    Product.find() // Just passing find() with no args returns everything
        .select('name price _id productImage') // select() is a method mongoose gives us to select only certain fields
        .exec()
        .then((docs) => {
            const response = {
                count: docs.length,
                products: docs.map(doc => {
                    return {
                        name: doc.name,
                        price: doc.price,
                        _id: doc._id,
                        productImage: doc.productImage,
                        request: {
                            /**
                             * For clarity we supply the request to get more info on this product
                             * on our API, the type of Request along with the URL, here it is hardcoded
                             * but this url can also come from an environment variable
                             */
                            type: 'GET',
                            url: 'http://localhost:3000/products/' + doc._id
                        }
                    }
                })
            }
            res.status(200).json(response);
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
});

/**
* router.post() is a method that will handle POST requests.
* 
* We can not only pass one handler (req, res, next) but as many as we want.
* Each one will just a a middleware that is handled sequentially before moving to the next
* We using the single() method suppllied by multer which tells multer that it will only need to pass one file to our uploads folder,
* we pass the field name that it will hold in this case, productImage. The multer package provides us with a new file property
* on our req object which we can log

* We parse the token in the header.Authorization property through the checkAuth middleware 
*/
router.post('/', checkAuth, upload.single('productImage'), (req, res, next) => {
    /**
     * The body-parser package allows us to access a body property
     * where we can easily access properties.
     */
    // const product = {
    //     name: req.body.name,
    //     price: req.body.price
    // }

    console.log(req.file);

    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        productImage: req.file.path
    });

    // We use mongoose's save() method to save product
    product.save().then(
        result => {
            console.log(result);
        /**
         * We use status code of 201 for a new resources that was created
         */
        res.status(201).json({
            message: 'Created product successfully',
            createdProduct: {
                name: result.name,
                price: result.price,
                _id: result._id,
                productImage: result.productImage,
                request: {
                    type: 'GET',
                    description: 'List all products',
                    url: 'http://localhost:3000/products/' + result._id
                }
            }
        })
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

/**
* Get a single product resource
* In Express we set variables in the router with a colon '/:<ANY_NAME>'
*/
router.get('/:productId', (req, res, next) => {
    // Extract our product ID
    const id = req.params.productId;
    
    /**
     * You can perform logic like this
     * at this point
     */
    // if (id === 'special') {
    //     res.status(200).json({
    //         message: 'You discovered a SPECIAL Id!',
    //         id: id
    //     });
    // } else {
    //     res.status(200).json({
    //         message: 'You passed an Id',
    //         id: id
    //     })
    // }

    Product.findById(id)
        .select('_id name price productImage')
        .exec() // exec() executes the method and converts the response to a promise
        .then( doc => {
            const response = {
                _id: doc._id,
                name: doc.name,
                price: doc.price,
                productImage: doc.productImage,
                requestAll: {
                    type: 'GET',
                    url: 'http://localhost:3000/products'
                }
            }

            if (doc) {
                res.status(200).json(response);
            } else {
                res.status(401).json({
                    message: 'No valid entry found for provided Product ID'
                })
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
});

// We add our patch/edit
router.patch('/:productId', checkAuth, (req, res, next) => {

    /**
     * Roughed out response
     */
    // res.status(200).json({
    //     message: 'Updated a product'
    // })
    const id = req.params.productId;

    /**
     * for update() first argument id the ID identifier and the second
     * is how we want to update it, all properties may not need to change.
     */

    // We need to check what property needs to update
    const updateOps = {};

    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }

    console.log('Update Props are:', JSON.stringify(updateOps));
    /**
     * So the body payload for the above will look like this if we want to update a price:
     * [{"propName": "price", "value": "15.88"}]
     */

    Product.updateOne({ _id: id }, { $set: updateOps }).exec()
    .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Product updated',
            request: {
                type: 'GET',
                url: 'http://localhost:3000/products/' + id
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        })
    });
});

// We add our delete route
router.delete('/:productId', checkAuth, (req, res, next) => {
    /**
     * Rough out return value
     */
    // res.status(200).json({
    //     message: 'Deleted product',
    //     productId: req.params.productId
    // })

    /**
     * Replace with Model and Mongoose method
     */
    
    const id = req.params.productId;
    Product.remove({_id: id}).exec()
        .then(result => {
            console.log(result);
            res.status(200).json({
                message: 'Product deleted',
                request: {
                    type: 'POST',
                    description: 'To add a new product',
                    url: 'http://localhost:3000/products',
                    body: { name: 'String', price: 'Number'}
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
});

module.exports = router;