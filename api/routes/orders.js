const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Order = require('../models/order');
const Product = require('../models/product');

/**
 * Check auth on all requests
 */
const checkAuth = require('../middleware/check-auth');

// Import controller for orders routes
const orderController = require('../controllers/orders');

 // Get all
router.get('/', checkAuth, orderController.orders_get_all);

// Create new
router.post('/', checkAuth, orderController.orders_create_new);

// Get details on a single order
router.get('/:orderId', checkAuth, orderController.orders_get_detail);

// Delete a single order
router.delete('/:orderId', checkAuth, orderController.orders_delete_order);

module.exports = router;