const Order = require('../models/order');
const Product = require('../models/product');
const mongoose = require('mongoose');

exports.orders_get_all = (req, res, next) => {
    /**
     * Temporarily used to mock out API
     */
    // res.status(200).json({
    // message: 'Orders were fetched'
    // })
    Order.find()
        .select('_id quantity product')
        .populate('product', 'name price') // We can pull details from another document model defined
        .exec()
        .then(docs => {
            res.status(200).json({
                count: docs.length,
                orders: docs.map(doc => {
                    return {
                        _id: doc._id,
                        product: doc.product,
                        request: {
                            type: 'GET',
                            description: 'Get info on this order',
                            url: 'http://localhost:3000/orders/' + doc._id
                        }
                    }
                })
            })
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        })
}

exports.orders_create_new = (req, res, next) => {

    // Find Product, if one exists proceed with creating order
    Product.findById(req.body.productId)
        .then(product => {
            const order = new Order({
                _id: mongoose.Types.ObjectId(),
                product: product._id,
                quantity: req.body.quantity
            });
            
            order.save()
                .then(doc => {
                    res.status(201).json({
                        message: 'Order created',
                        orderCreated: doc,
                        request: {
                            type: 'GET',
                            endpoint: 'http://localhost:3000/orders/' + doc._id
                        }
                    });
                })
                .catch(err => {
                    res.status(500).json({
                        message: 'Could not save order',
                        error: err
                    })
                })
        })
        .catch(err => {
            // Product does not exist so cannot create an order
            console.log(err);
            res.status(500).json({
                message: 'Product does not exist',
                error: err
            })
        });
}

exports.orders_get_detail = (req, res, next) => {

    const id = req.params.orderId;

    Order.findById(id)
        .select('_is product quantity')
        .exec()
        .then(doc => {

            // Return a 404 if order is not found
            if(!doc) {
                return res.status(404).json({
                    message: 'Order not found'
                })
            }
            const response = {
                _id: doc._id,
                product: doc.product,
                quantity: doc.quantity,
                request: {
                    type: 'GET',
                    description: 'Get all orders',
                    endpoint: 'http://localhost:3000/orders'
                }
            }
            res.status(200).json(response);
        })
        .catch(err => {
            const response = {
                message: 'Error Fetching Order',
                error: err
            }

            res.status(500).json(response);
        })
    
    
    /**
     * Rough initial response
     */
    // res.status(200).json({
    // message: 'Orders details',
    // orderId: req.params.orderId
    // })
}

exports.orders_delete_order = (req, res, next) => {

    

    Order.findById(req.params.orderId)
        .exec()
        .then(doc => {

            // return 404 if cannot find order
            if (!doc) {
                return res.status(404).json({
                    message: 'Cannot find order'
                })
            }

            Order.deleteOne({ _id: doc._id })
                .exec()
                .then(result => {

                    res.status(200).json({
                        message: 'Order deleted',
                        request: {
                            type: 'POST',
                            description: 'To add a new order',
                            endpoint: 'http://localhost:3000/orders',
                            body: { productId: 'String', quantity: 'Number' }
                        }
                    });
                })
                .catch(err => {
                    res.status(500).json({
                        message: 'Could not delete order',
                        error: err
                    })
                });
        })
        .catch(err => {
            res.status(500).json({
                message: 'Error occured when deleting order'
            })
        })


    /**
     * Rough out initial response
     */
    // res.status(200).json({
    //     message: 'Order deleted',
    //     orderId: req.params.orderId
    // })
}