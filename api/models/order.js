const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    product : { type: mongoose.Schema.Types.ObjectId, ref: 'Products', required: true },
    quantity: { type: Number, default: 1 }
 });

 /**
  * For the product property we use the ref property to reference another Model defined,
  * this way we can reference information about the product if needed.
  */

 module.exports = mongoose.model('Order', orderSchema);