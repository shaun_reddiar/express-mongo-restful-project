const mongoose = require('mongoose');

/**
 * We use the unique: true property to optimise the field so that mongoose knows that this will be search etc, it won't add validation
 * We use the match property to pass some regex to ensure that a valid email is used
 */

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        required: true,
        unique: true,
        match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },
    password: { type: String, required: true }
 });

 module.exports = mongoose.model('User', userSchema);