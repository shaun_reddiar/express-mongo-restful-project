/**
 * In this file we will define what a product will look like in
 * our database
 */

 const mongoose = require('mongoose');

 const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    price: { type: Number, required: true },
    productImage: { type: String, required: true }
 });

/**
 * Model function takes two arguments:
 * name: Can be anything, used internally - convention is to use Titlecase
 * schema: scheme defined above to use with this model
 */
 module.exports = mongoose.model('Products', productSchema);