# Carry on from here:
User Sign in via Token:
https://www.youtube.com/watch?v=0D5EEKH97NA&list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q&index=12

## Steps to building our API
1. Plan all resources you require and the methods you will need for each (POST, GET, PATCH, DELETE etc)
2. Build your environment using Node, Express Morgan
3. Rough out end points as per plan, simply returning messages confirming each returns successfully
4. Error handle all endpoints not covered in your API
5. Use body-parser to be able to extract the body from the requests and handle CORS Errors

    - When testing POSTS in Postman, remember to set your URL, click body and click Raw in the list below
        In your object you pass in the body, remember to set everything as strings ("key": "Value")

    - CORS stand for: Cross-Origin Resource Sharing: Its Security measures on browsers:
        -   When a request is sent to a server on the same domain/location it will succeed - like http://localhost
            For a SPA this is almost never the case, client and server very often have differnt locations, even a PORT number is considered an origin, so it would still be considered as different if the PORT numbers are not the same. We resolve this by sending info in our header of our request
6. Create a database for our resources using MongoDB and Mongoose:
    https://www.youtube.com/watch?v=WDrU305J1yw
    
    - Create Mongoose models - define what objects we define in the database look like via a model, the resultiung models will contain methods to provide required CRUD functionality

    - Replace the roughed out end points created with teh proper ones using the Models created for each resource and test on POSTMAN

    - Add Mongoose Validation - https://www.youtube.com/watch?v=CMDsTMV2AgI
        - Only valid data should be saved to the database
            - If we don't add a price to a product it should not save
            - If we use a word instead of a price ammount it should not save
            - If we don't enter a name it should not save
            - If we add a new property in our body, it shoudl not save
        - We need to endure that our endpoint responses are correct
            - The responses we get thus far may make sense to us, but for a new developer on the project, it wont, they won't know any other endpoints that may exist, this all needs to be included in our responses
            - We may also want to send some meta data, like the amount of data fetched, so we may not only want to send the response as we get it, we will need to shape it into somethign a bit more understandable, we can do this by filtering out fields that we don't need with the **select()** method mongoose gives us
7. Add Our Orders:
    - Create Model for orders
    - Add Mongoose methods for all CRUD operations
    - Deal with the case were an invalid product ID is created. It should not still save the order
    - Consider fetching items that do not exist, return 404 errors in Promise then() statements

8. Consider populating more data that is returned on queries on existing endpoints or consider adding more endpoints that may be required. This is because sometimes, you have related documents in your MongoDB. https://www.youtube.com/watch?v=3p0wmR973Fw&list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q&index=9

9. Add image upload: https://www.youtube.com/watch?v=srPXMt1Q0nY&list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q&index=10
    - Add a file/image upload to the products endpoint, it will require the this endpoint accepting not a json body supplied by the body-parser package used by the other endpoints, but a multi-part form body so that we can pass data like forms fields with names, price etc as well as files.
    To be able to pass form data we will need install a new package called multer
    
        `npm install --save multer`
        
        Like `body-parser`, the `multer` package will pass not json bodies but form data bodies.
        When testing in Postman, be sure to:
        - Set body to form-data
        - Add key pair values as per you data including a file field called productImage
        - On your header deslect content-type json
        - Store the file entry in the database so that we can fetch the image
            - This is handled in the Product Model
            - Add the productImage url to all urls that return product info to include this url
            - if you add the uploads address in the browser after localhost:3000 you will see that it throws an error because
                the uploads folder is not publically available. We solve this by making the uploads folder publicall available
                with a express method called `express.static('uploads')`

10. Add User Sign up and Authentication: https://www.youtube.com/watch?v=_EP2qCmLzSE&list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q&index=11
    - For a RESTful API there is no session due to the API being stateless. No user is "connected" via a session as with other NON-REST methods, so we need to pass and validate a user by means of a JSON WEB TOKN (JWT Token) which is issued to client from the server and used to validate requests.
    - Steps: 
        **Look at files in code base to see code**
        - Add a User Model to define what a user will look like - *models/user.js* 
        - Define the user routes by adding - *routes/user.js*
            - There will be three routes, *signup, delete* and *login* We won't need a logout route because there is no session to delete
            - The password should never be stored in RAW format, it needs to be encrypted to do this we will need a package called node.bcrypt.js:
                
                `npm install --save bcrypt`
            
                This package will hash our password before it is saved and ensure that it cannot be unhashed via Dictionary Tables
        - Import the user route into the app.js file
        - When testing on Postman, remember to set body to RAW and check the content-type Application/json checkbox
        - Edge-case - Ensure that a user with an email address can only be saved once and that duplicate users cannot be created with teh same email address
        - Add delete user route and ensure that we check if that user exists before we remove it
        - Add regex to ensure valid email addresses are saved

11. Implement user signin via token: https://www.youtube.com/watch?v=0D5EEKH97NA&list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q&index=12
    - We will receive a JWT token from the server, store it and attach this token to all requests. The server will validate this token with each request
    - implement the *login* route on the user endpoint
    - We will use the bcrypt packages *compare()* method to hash and compare the request password against the stored user password
    - To get our JWT token we will use a package called *node-jsonwebtoken*
        - `npm install --save jsonwebtoken`

12. Send our token with requests to resources that we want to protect and verifying that token
    - There are some routes that you wouldn't want to proect, like the pist of your products, or your products details page. But the following may need route protection:
        - creating, updating or deleting products
        - All order related routes
    - We will add middleware to check whether the user has permission to a route before that route is processed, the code for this is in:
        - `middleware/check-auth.js`
    - When testing in Postman, set Body to raw, attempt to pass email and password, copy the returned web token, Under headers, set Content-type to *application/json* then add a field called *Authorization: Bearer <YOUR-COPIED-TOKEN>*. So we add the token to our Authorization header, not the request body
    - Add the checkAuth middlware to all routes that need it ie. all routes that need to be protected

13. Add Controllers to clean up route files: https://www.youtube.com/watch?v=ucuNgSOFDZ0&list=PL55RiY5tL51q4D-B63KBnygU6opNPFk_q&index=14
    API is virtually done now, a final clean up. Our route files are relatively big, we can abstract all the logic from each route into a respective file in a controllers folder. This will move this into a bit of a cleaner file structure. Example done for orders only as an example. `controllers/order.js`
    We could also look into refinign the environment variables: https://dev.to/numtostr/environment-variables-in-node-js-the-right-way-15ad
