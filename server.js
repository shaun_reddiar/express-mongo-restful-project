/**
 * Here we write all our code to spin up our NodeJS server
 */
const http = require('http'); // This provides the functionality required from Node to spin up our server 
const app = require('./app'); // Our express applicatiion

/**
 * Here we define a port from which the project will run. 
 * We can either hard code that port number or you can get it via
 * an environment variable as in this case. process.env accesses Nodes environment variables.
 * This will be set on the server that you deploy it on. If its not set on the server we will default
 * to use 3000
 */
const port = process.env.PORT || 3000 

/**
 * With this we create the server using the http package. We pass a listener or function that gets executed
 * everytime we get a request. We pass in our 'app' Express application. 
 */
const server = http.createServer(app);

/**
 * Start server and listen on the port
 */
server.listen(port);